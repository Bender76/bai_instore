
function goHome() {
		document.getElementById('page').innerHTML = '';
		
}

function displayAfterLogout() {
	goHome();
}

function displayAfterLogin() {
}

function addOrderItem() {
	
	showModal();
}

// Stolen from stack overflow
function camelize(str) {
  return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function(letter, index) {
    return index == 0 ? letter.toLowerCase() : letter.toUpperCase();
  }).replace(/\s+/g, '');
}